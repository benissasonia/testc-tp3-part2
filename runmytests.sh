#!/bin/bash

# This is a sample test script
# Replace the following line with the actual command to run your tests

./jeu  # Assuming mybinary is the executable produced during the build stage

# Add any other test commands or assertions as needed
# For example, you might have commands like:
# ./mybinary arg1 arg2
# ./mybinary --flag
# ...

# Exit with appropriate status based on test results
# For example, if all tests pass, exit with 0; otherwise, exit with a non-zero status
# Replace the following line with your actual exit logic

exit 0
