// main.c
#include "jeu.h"
#include <stdio.h>

int main() {
    char choixJoueur;
    srand(time(NULL));

    printf("Choisissez (R) Roche, (P) Papier, ou (C) Ciseaux: ");
    scanf(" %c", &choixJoueur);

    if (choixJoueur != 'R' && choixJoueur != 'P' && choixJoueur != 'C') {
        printf("Choix invalide. Veuillez choisir R, P ou C.\n");
        return 1;
    }

    char choixOrdinateur = hasard();
    comparaison(choixJoueur, choixOrdinateur);

    return 0;
}
