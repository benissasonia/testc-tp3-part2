// jeu.c
#include "jeu.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char hasard() {
    int randomNum = rand() % 3;
    char choix;

    switch (randomNum) {
        case 0:
            choix = 'R';
            break;
        case 1:
            choix = 'P';
            break;
        case 2:
            choix = 'C';
            break;
    }

    return choix;
}

void comparaison(char joueur, char ordinateur) {
    printf("Joueur choisit: %c\n", joueur);
    printf("Ordinateur choisit: %c\n", ordinateur);

    if (joueur == ordinateur) {
        printf("Égalité!\n");
    } else if ((joueur == 'R' && ordinateur == 'C') ||
               (joueur == 'P' && ordinateur == 'R') ||
               (joueur == 'C' && ordinateur == 'P')) {
        printf("Le joueur gagne!\n");
    } else {
        printf("L'ordinateur gagne!\n");
    }
}
