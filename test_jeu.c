// test_jeu.c
#include "unity.h"
#include "jeu.h"

void test_hasard() {
    char resultat = hasard();
    TEST_ASSERT_TRUE(resultat == 'R' || resultat == 'P' || resultat == 'C');
}

void test_comparaison() {
    // Testez les cas que vous trouvez pertinents
    // ...

    // Exemple de test (à adapter en fonction de votre logique de jeu)
    TEST_ASSERT_EQUAL_STRING("Le joueur gagne!", "Le joueur gagne!");
}

int main() {
    UNITY_BEGIN();

    // L'ordre des tests importe
    RUN_TEST(test_hasard);
    RUN_TEST(test_comparaison);

    return UNITY_END();
}
